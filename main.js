async function getUsers() {
    const response = await fetch("https://jsonplaceholder.typicode.com/users");
    const userList = await response.json();
    
    // création d'un nouveau tableau qui récupère uniquement le nom et tous les éléments contenus dans adresse
    const userAddress = userList.map(user => {
        return [
            "Nom : " + user.name, 
            "Rue : " + user.address.street, 
            "Ville : " + user.address.city, 
            "Code Postal : " + user.address.zipcode]
    })
    // Affichage du nouveau tableau
    console.log(userAddress)
};

getUsers();